module.exports = {
  // ...other vue-cli plugin options...
  pwa: {
    workboxPluginMode: "GenerateSW",
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      //swSrc: "src/sw.js",
      //swDest: "service-worker.js",
    },
  },
};
